package deloitte.academy.lesson01.controller;

import java.util.ArrayList;
import java.util.function.Consumer;

import deloitte.academy.lesson01.model.Entity;
import deloitte.academy.lesson01.tester.Test;
/**
 * 
 * @author Victor Alvarado
 * @version 1
 * @since 11/03/20
 *
 */
public class ControllerProduct {

	/**
	 * 
	 * @param lista    son los objetos
	 * @param producto es donde se van a agregar los objetos
	 * @return regresa el mensaje, ya sea de exito o error
	 */
   
	public static String agregarObjetos(Entity atributo)  /*Se a�aden objetos a la lista */
	{	
	
		String mensaje = " ";
		try {
			Test.listaAtributo.add(atributo);
			mensaje = "Exito";
		} catch (Exception e) {
			mensaje = "Error";
		}

		return mensaje;

	}
	
	/**
	 * 
	 * @param listaAtributos regresa la lista d elos atributos
	 * @param atributo1 es uno de los atributos
	 * @return regresa el atributo
	 */
/*
	public static String Buscar() {
		String registros= " ";
	
		
	    Entity.listaAtributo.forEach(n -> System.out.println(n));	
	   
	    return registros;
	    
	}
	*/
   
	public Entity buscarAtributo(String atributo1) {
		try {
			
			Test.listaAtributo.forEach(new Consumer <Entity>() {
				@Override
				public void accept(Entity entity) {
					if (entity.getAtributo1()==atributo1) {
						Test.busqueda= entity;
					}
				}
			});
		
		} catch (Exception e) {
			System.out.println("Problem");
		}
		
		return Test.busqueda;
	}
	
}
	 
