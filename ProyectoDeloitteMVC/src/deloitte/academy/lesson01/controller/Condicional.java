package deloitte.academy.lesson01.controller;
/**
 * 
 * @author Victor Alvarado
 * @version 1
 * @since 11/03/20
 *
 */
public class Condicional {
	/**
	 * 
	 * @param i el primer valor a comparar
	 * @param j el segundo valor a comparar
	 * @return regresa el resultado
	 */
	
	/**
	 * 
	 * @param e el primer valor a comparar
	 * @param d el segundo valor a comparar
	 * @return regresa el resultado
	 */
	public static int operadorTernario2(int e, int d) {

		int numeroMenor = 0;
		numeroMenor = e < d ? e : d;
		return numeroMenor;
	}

	/**
	 * 
	 * @param x el primer valor a comparar
	 * @param y el segundo valor a comparar
	 * @param z el tercer valor a comparar
	 * @return regresa el resultado
	 */
	public static int operadorTernario3(int x, int y, int z) {

		int numeroMayor3 = 0;
		numeroMayor3 = ((x > y) ? (x > z) ? z : z : (y > z) ? y : z);
		return numeroMayor3;
	}

	/**
	 * 
	 * @param h el primer valor a comparar
	 * @param k el segundo valor a comparar
	 * @return regresa el valor
	 */
	public static int ejemploIf(int h, int k) {
		int resultadoIf = 0;

		if (k > h) {
			resultadoIf = k;
		} else {
			resultadoIf = h;
		}

		return resultadoIf;
	}

	/**
	 * 
	 * @param f el primer valor
	 * @param g el segundo valor
	 * @param u el tercer valor
	 * @return retorna el resultado
	 */

	public static String ejemploIfElse(int f, int g, int u) {
		String resultadoIfElse = " ";
		{
			if (f == 0)
				resultadoIfElse = "f es igual a 0";
			else if (g == 0) {
				resultadoIfElse = "g es igual a 0 ";
			} else if (u == 0) {
				resultadoIfElse = "u es igual a 0";
			} else {
				resultadoIfElse = "Ninguno es 0";
			}

		}
		return resultadoIfElse;
	}
}
