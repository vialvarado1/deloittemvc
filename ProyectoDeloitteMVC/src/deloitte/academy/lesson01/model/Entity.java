package deloitte.academy.lesson01.model;

/**
 * 
 * @author Victor Alvarado
 * @since 11/03/20
 * @version 1
 *
 */

public class Entity {
	private int ide;
	private double weight;
	private double cantidad;
	private double size;
	private double volumen;
	private String categoria;
	private String origen;
	private String color;
	private String descripcion;
	private String atributo1;
	private String atributo2;
	private String atributo3;
	private String atributo4;
	private String atributo5;
	private String atributo6;
	private String atributo7;
	private String atributo8;
	private String atributo9;
	private String atributo10;

	/**
	 */

	public Entity() {
		super();
		this.ide = ide;
		this.weight = weight;
		this.cantidad = cantidad;
		this.size = size;
		this.volumen = volumen;
		this.categoria = categoria;
		this.origen = origen;
		this.color = color;
		this.descripcion = descripcion;
		this.atributo1 = atributo1;
		this.atributo2 = atributo2;
		this.atributo3 = atributo3;
		this.atributo4 = atributo4;
		this.atributo5 = atributo5;
		this.atributo6 = atributo6;
		this.atributo7 = atributo7;
		this.atributo8 = atributo8;
		this.atributo9 = atributo9;
		this.atributo10 = atributo10;
	}

	/**
	 * 
	 * @return
	 */

	
	public int getIde() { 
		return ide;
	}

	public Entity(int ide, double weight, double cantidad, double size, double volumen, String categoria, String origen,
			String color, String descripcion, String atributo1, String atributo2, String atributo3, String atributo4,
			String atributo5, String atributo6, String atributo7, String atributo8, String atributo9,
			String atributo10) {
		super();
		this.ide = ide;
		this.weight = weight;
		this.cantidad = cantidad;
		this.size = size;
		this.volumen = volumen;
		this.categoria = categoria;
		this.origen = origen;
		this.color = color;
		this.descripcion = descripcion;
		this.atributo1 = atributo1;
		this.atributo2 = atributo2;
		this.atributo3 = atributo3;
		this.atributo4 = atributo4;
		this.atributo5 = atributo5;
		this.atributo6 = atributo6;
		this.atributo7 = atributo7;
		this.atributo8 = atributo8;
		this.atributo9 = atributo9;
		this.atributo10 = atributo10;
	}

	public void setIde(int ide) {
		this.ide = ide;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getVolumen() {
		return volumen;
	}

	public void setVolumen(double volumen) {
		this.volumen = volumen;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getAtributo1() {
		return atributo1;
	}

	public void setAtributo1(String atributo1) {
		this.atributo1 = atributo1;
	}

	public String getAtributo2() {
		return atributo2;
	}

	public void setAtributo2(String atributo2) {
		this.atributo2 = atributo2;
	}

	public String getAtributo3() {
		return atributo3;
	}

	public void setAtributo3(String atributo3) {
		this.atributo3 = atributo3;
	}

	public String getAtributo4() {
		return atributo4;
	}

	public void setAtributo4(String atributo4) {
		this.atributo4 = atributo4;
	}

	public String getAtributo5() {
		return atributo5;
	}

	public void setAtributo5(String atributo5) {
		this.atributo5 = atributo5;
	}

	public String getAtributo6() {
		return atributo6;
	}

	public void setAtributo6(String atributo6) {
		this.atributo6 = atributo6;
	}

	public String getAtributo7() {
		return atributo7;
	}

	public void setAtributo7(String atributo7) {
		this.atributo7 = atributo7;
	}

	public String getAtributo8() {
		return atributo8;
	}

	public void setAtributo8(String atributo8) {
		this.atributo8 = atributo8;
	}

	public String getAtributo9() {
		return atributo9;
	}

	public void setAtributo9(String atributo9) {
		this.atributo9 = atributo9;
	}

	public String getAtributo10() {
		return atributo10;
	}

	public void setAtributo10(String atributo10) {
		this.atributo10 = atributo10;
	}

	

		
	}



